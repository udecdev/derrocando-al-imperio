

 /* =====================================================================================

SCORM wrapper v1.1.7 by Philip Hutchison, May 2008 (http://pipwerks.com).

Copyright (c) 2008 Philip Hutchison
MIT-style license. Full license text can be found at 
http://www.opensource.org/licenses/mit-license.php

This wrapper is designed to work with both SCORM 1.2 and SCORM 2004.

Based on APIWrapper.js, created by the ADL and Concurrent Technologies
Corporation, distributed by the ADL (http://www.adlnet.gov/scorm).

SCORM.API.find() and SCORM.API.get() functions based on ADL code,
modified by Mike Rustici (http://www.scorm.com/resources/apifinder/SCORMAPIFinder.htm),
further modified by Philip Hutchison

======================================================================================== */


var pipwerks = {};									//pipwerks 'namespace' helps ensure no conflicts with possible other "SCORM" variables
pipwerks.UTILS = {};								//For holding UTILS functions
pipwerks.debug = { isActive: true }; 				//Enable (true) or disable (false) for debug mode

pipwerks.SCORM = {									//Define the SCORM object
    version:    null,              					//Store SCORM version.
	handleCompletionStatus: true,					//Whether or not the wrapper should automatically handle the initial completion status
	handleExitMode: true,							//Whether or not the wrapper should automatically handle the exit mode
    API:        { handle: null, 
				  isFound: false },					//Create API child object
    connection: { isActive: false },				//Create connection child object
    data:       { completionStatus: null,
				  exitStatus: null },				//Create data child object
    debug:      {}                 					//Create debug child object
};



/* --------------------------------------------------------------------------------
   pipwerks.SCORM.isAvailable
   A simple function to allow Flash ExternalInterface to confirm 
   presence of JS wrapper before attempting any LMS communication.

   Parameters: none
   Returns:    Boolean (true)
----------------------------------------------------------------------------------- */

pipwerks.SCORM.isAvailable = function(){
	return true;     
};



// ------------------------------------------------------------------------- //
// --- SCORM.API functions ------------------------------------------------- //
// ------------------------------------------------------------------------- //


/* -------------------------------------------------------------------------
   pipwerks.SCORM.API.find(window)
   Looks for an object named API in parent and opener windows
   
   Parameters: window (the browser window object).
   Returns:    Object if API is found, null if no API found
---------------------------------------------------------------------------- */

pipwerks.SCORM.API.find = function(win){

    var API = null,
		findAttempts = 0,
        findAttemptLimit = 500,
		traceMsgPrefix = "SCORM.API.find",
		trace = pipwerks.UTILS.trace,
		scorm = pipwerks.SCORM;

    while ((!win.API && !win.API_1484_11) &&
           (win.parent) &&
           (win.parent != win) &&
           (findAttempts <= findAttemptLimit)){

                findAttempts++; 
                win = win.parent;

    }

	if(scorm.version){											//If SCORM version is specified by user, look for specific API
	
		switch(scorm.version){
			
			case "2004" : 
			
				if(win.API_1484_11){
			
					API = win.API_1484_11;
				 
				} else {
					
					trace(traceMsgPrefix +": SCORM version 2004 was specified by user, but API_1484_11 cannot be found.");
					
				}
				
				break;
				
			case "1.2" : 
			
				if(win.API){
			
					API = win.API;
				 
				} else {
					
					trace(traceMsgPrefix +": SCORM version 1.2 was specified by user, but API cannot be found.");
					
				}
				
				break;
			
		}
		
	} else {													//If SCORM version not specified by user, look for APIs
		
		if(win.API_1484_11) {									//SCORM 2004-specific API.
	
			scorm.version = "2004";								//Set version
			API = win.API_1484_11;
		 
		} else if(win.API){										//SCORM 1.2-specific API
			  
			scorm.version = "1.2";								//Set version
			API = win.API;
		 
		}

	}

	if(API){
		
		trace(traceMsgPrefix +": API found. Version: " +scorm.version);
		trace("API: " +API);

	} else {
		
		trace(traceMsgPrefix +": Error finding API. \nFind attempts: " +findAttempts +". \nFind attempt limit: " +findAttemptLimit);
		
	}
	
    return API;

};


/* -------------------------------------------------------------------------
   pipwerks.SCORM.API.get()
   Looks for an object named API, first in the current window's frame
   hierarchy and then, if necessary, in the current window's opener window
   hierarchy (if there is an opener window).

   Parameters:  None. 
   Returns:     Object if API found, null if no API found
---------------------------------------------------------------------------- */

pipwerks.SCORM.API.get = function(){

    var API = null,
		win = window,
		find = pipwerks.SCORM.API.find,
		trace = pipwerks.UTILS.trace; 
     
    if(win.parent && win.parent != win){ 
    
        API = find(win.parent); 
        
    } 
     
    if(!API && win.top.opener){ 
    
        API = find(win.top.opener); 
        
    } 
     
    if(API){  
    
        pipwerks.SCORM.API.isFound = true;
        
    } else {
    
        trace("API.get failed: Can't find the API!");
                               
    }
     
    return API;

};
          

/* -------------------------------------------------------------------------
   pipwerks.SCORM.API.getHandle()
   Returns the handle to API object if it was previously set

   Parameters:  None.
   Returns:     Object (the pipwerks.SCORM.API.handle variable).
---------------------------------------------------------------------------- */

pipwerks.SCORM.API.getHandle = function() {
	
	var API = pipwerks.SCORM.API;
     
    if(!API.handle && !API.isFound){
     
        API.handle = API.get();
     
    }
     
    return API.handle;

};
     


// ------------------------------------------------------------------------- //
// --- pipwerks.SCORM.connection functions --------------------------------- //
// ------------------------------------------------------------------------- //


/* -------------------------------------------------------------------------
   pipwerks.SCORM.connection.initialize()
   Tells the LMS to initiate the communication session.

   Parameters:  None
   Returns:     Boolean
---------------------------------------------------------------------------- */

pipwerks.SCORM.connection.initialize = function(){
               
    var success = false,
		scorm = pipwerks.SCORM,
		completionStatus = pipwerks.SCORM.data.completionStatus,
		trace = pipwerks.UTILS.trace,
		makeBoolean = pipwerks.UTILS.StringToBoolean,
		debug = pipwerks.SCORM.debug,
		traceMsgPrefix = "SCORM.connection.initialize ";

    trace("connection.initialize called.");

    if(!scorm.connection.isActive){

        var API = scorm.API.getHandle(),
            errorCode = 0;
          
        if(API){
               
			switch(scorm.version){
				case "1.2" : success = makeBoolean(API.LMSInitialize("")); break;
				case "2004": success = makeBoolean(API.Initialize("")); break;
			}
			
            if(success){
            
				//Double-check that connection is active and working before returning 'true' boolean
				errorCode = debug.getCode();
				
				if(errorCode !== null && errorCode === 0){
					
	                scorm.connection.isActive = true;
					
					if(scorm.handleCompletionStatus){
						
						//Automatically set new launches to incomplete 
						completionStatus = pipwerks.SCORM.status("get");
						
						if(completionStatus){
						
							switch(completionStatus){
								
								//Both SCORM 1.2 and 2004
								case "not attempted": pipwerks.SCORM.status("set", "incomplete"); break;
								
								//SCORM 2004 only
								case "unknown" : pipwerks.SCORM.status("set", "incomplete"); break;
								
								//Additional options, presented here in case you'd like to use them
								//case "completed"  : break;
								//case "incomplete" : break;
								//case "passed"     : break;	//SCORM 1.2 only
								//case "failed"     : break;	//SCORM 1.2 only
								//case "browsed"    : break;	//SCORM 1.2 only
								
							}
							
						}
						
					}
				
				} else {
					
					success = false;
					trace(traceMsgPrefix +"failed. \nError code: " +errorCode +" \nError info: " +debug.getInfo(errorCode));
					
				}
                
            } else {
				
				errorCode = debug.getCode();
            
				if(errorCode !== null && errorCode !== 0){

					trace(traceMsgPrefix +"failed. \nError code: " +errorCode +" \nError info: " +debug.getInfo(errorCode));
					
				} else {
					
					trace(traceMsgPrefix +"failed: No response from server.");
				
				}
            }
              
        } else {
          
            trace(traceMsgPrefix +"failed: API is null.");
     
        }
          
    } else {
     
          trace(traceMsgPrefix +"aborted: Connection already active.");
          
     }

     return success;

};


/* -------------------------------------------------------------------------
   pipwerks.SCORM.connection.terminate()
   Tells the LMS to terminate the communication session

   Parameters:  None
   Returns:     Boolean
---------------------------------------------------------------------------- */

pipwerks.SCORM.connection.terminate = function(){
     
    var success = false,
		scorm = pipwerks.SCORM,
		exitStatus = pipwerks.SCORM.data.exitStatus,
		completionStatus = pipwerks.SCORM.data.completionStatus,
		trace = pipwerks.UTILS.trace,
		makeBoolean = pipwerks.UTILS.StringToBoolean,
		debug = pipwerks.SCORM.debug,
		traceMsgPrefix = "SCORM.connection.terminate ";


    if(scorm.connection.isActive){
          
        var API = scorm.API.getHandle(),
            errorCode = 0;
               
        if(API){
     
	 		if(scorm.handleExitMode && !exitStatus){
				
				if(completionStatus !== "completed" && completionStatus !== "passed"){
			
					switch(scorm.version){
						case "1.2" : success = scorm.set("cmi.core.exit", "suspend"); break;
						case "2004": success = scorm.set("cmi.exit", "suspend"); break;
					}
					
				} else {
					
					switch(scorm.version){
						case "1.2" : success = scorm.set("cmi.core.exit", "logout"); break;
						case "2004": success = scorm.set("cmi.exit", "normal"); break;
					}
					
				}
			
			}
	 
			switch(scorm.version){
				case "1.2" : success = makeBoolean(API.LMSFinish("")); break;
				case "2004": success = makeBoolean(API.Terminate("")); break;
			}
               
            if(success){
                    
                scorm.connection.isActive = false;
               
            } else {
                    
                errorCode = debug.getCode();
                trace(traceMsgPrefix +"failed. \nError code: " +errorCode +" \nError info: " +debug.getInfo(errorCode));
   
            }
               
        } else {
          
            trace(traceMsgPrefix +"failed: API is null.");
     
        }
          
    } else {
     
        trace(traceMsgPrefix +"aborted: Connection already terminated.");

    }

    return success;

};



// ------------------------------------------------------------------------- //
// --- pipwerks.SCORM.data functions --------------------------------------- //
// ------------------------------------------------------------------------- //


/* -------------------------------------------------------------------------
   pipwerks.SCORM.data.get(parameter)
   Requests information from the LMS.

   Parameter: parameter (string, name of the SCORM data model element)
   Returns:   string (the value of the specified data model element)
---------------------------------------------------------------------------- */

pipwerks.SCORM.data.get = function(parameter){

    var value = null,
		scorm = pipwerks.SCORM,
		trace = pipwerks.UTILS.trace,
		debug = pipwerks.SCORM.debug,
		traceMsgPrefix = "SCORM.data.get(" +parameter +") ";

    if(scorm.connection.isActive){
          
        var API = scorm.API.getHandle(),
            errorCode = 0;
          
          if(API){
               
			switch(scorm.version){
				case "1.2" : value = API.LMSGetValue(parameter); break;
				case "2004": value = API.GetValue(parameter); break;
			}
			
            errorCode = debug.getCode();
               
            //GetValue returns an empty string on errors
            //Double-check errorCode to make sure empty string
            //is really an error and not field value
            if(value !== "" && errorCode === 0){
			   
				switch(parameter){
					
					case "cmi.core.lesson_status": 
					case "cmi.completion_status" : scorm.data.completionStatus = value; break;
					
					case "cmi.core.exit": 
					case "cmi.exit" 	: scorm.data.exitStatus = value; break;
					
				}
               
            } else {
				
                trace(traceMsgPrefix +"failed. \nError code: " +errorCode +"\nError info: " +debug.getInfo(errorCode));
								
			}
          
        } else {
          
            trace(traceMsgPrefix +"failed: API is null.");
     
        }
          
    } else {
     
        trace(traceMsgPrefix +"failed: API connection is inactive.");

    }
	
	trace(traceMsgPrefix +" value: " +value);
	
    return String(value);

};
          
          
/* -------------------------------------------------------------------------
   pipwerks.SCORM.data.set()
   Tells the LMS to assign the value to the named data model element.
   Also stores the SCO's completion status in a variable named
   pipwerks.SCORM.data.completionStatus. This variable is checked whenever
   pipwerks.SCORM.connection.terminate() is invoked.

   Parameters: parameter (string). The data model element
               value (string). The value for the data model element
   Returns:    Boolean
---------------------------------------------------------------------------- */

pipwerks.SCORM.data.set = function(parameter, value){

    var success = false,
		scorm = pipwerks.SCORM,
		trace = pipwerks.UTILS.trace,
		makeBoolean = pipwerks.UTILS.StringToBoolean,
		debug = pipwerks.SCORM.debug,
		traceMsgPrefix = "SCORM.data.set(" +parameter +") ";
		
		
    if(scorm.connection.isActive){
          
        var API = scorm.API.getHandle(),
            errorCode = 0;
               
        if(API){
               
			switch(scorm.version){
				case "1.2" : success = makeBoolean(API.LMSSetValue(parameter, value)); break;
				case "2004": success = makeBoolean(API.SetValue(parameter, value)); break;
			}
			
            if(success){
				
				if(parameter === "cmi.core.lesson_status" || parameter === "cmi.completion_status"){
					
					scorm.data.completionStatus = value;
					
				}
				
			} else {

                trace(traceMsgPrefix +"failed. \nError code: " +errorCode +". \nError info: " +debug.getInfo(errorCode));

            }
               
        } else {
          
            trace(traceMsgPrefix +"failed: API is null.");
     
        }
          
    } else {
     
        trace(traceMsgPrefix +"failed: API connection is inactive.");

    }
     
    return success;

};
          

/* -------------------------------------------------------------------------
   pipwerks.SCORM.data.save()
   Instructs the LMS to persist all data to this point in the session

   Parameters: None
   Returns:    Boolean
---------------------------------------------------------------------------- */

pipwerks.SCORM.data.save = function(){

    var success = false,
		scorm = pipwerks.SCORM,
		trace = pipwerks.UTILS.trace,
		makeBoolean = pipwerks.UTILS.StringToBoolean,
		traceMsgPrefix = "SCORM.data.save failed";


    if(scorm.connection.isActive){

        var API = scorm.API.getHandle();
          
        if(API){
          
			switch(scorm.version){
				case "1.2" : success = makeBoolean(API.LMSCommit("")); break;
				case "2004": success = makeBoolean(API.Commit("")); break;
			}
			
        } else {
          
            trace(traceMsgPrefix +": API is null.");
     
        }
          
    } else {
     
        trace(traceMsgPrefix +": API connection is inactive.");

    }

    return success;

};


pipwerks.SCORM.status = function (action, status){
	
    var success = false,
		scorm = pipwerks.SCORM,
		trace = pipwerks.UTILS.trace,
		traceMsgPrefix = "SCORM.getStatus failed",
		cmi = "";

	if(action !== null){
		
		switch(scorm.version){
			case "1.2" : cmi = "cmi.core.lesson_status"; break;
			case "2004": cmi = "cmi.completion_status"; break;
		}
		
		switch(action){
			
			case "get": success = pipwerks.SCORM.data.get(cmi); break;
			
			case "set": if(status !== null){
				
							success = pipwerks.SCORM.data.set(cmi, status);
							
						} else {
							
							success = false;
							trace(traceMsgPrefix +": status was not specified.");
							
						}
						
						break;
						
			default	  : success = false;
						trace(traceMsgPrefix +": no valid action was specified.");
						
		}
		
	} else {
		
		trace(traceMsgPrefix +": action was not specified.");
		
	}
	
	return success;

};


// ------------------------------------------------------------------------- //
// --- pipwerks.SCORM.debug functions -------------------------------------- //
// ------------------------------------------------------------------------- //


/* -------------------------------------------------------------------------
   pipwerks.SCORM.debug.getCode
   Requests the error code for the current error state from the LMS

   Parameters: None
   Returns:    Integer (the last error code).
---------------------------------------------------------------------------- */

pipwerks.SCORM.debug.getCode = function(){
     
    var API = pipwerks.SCORM.API.getHandle(),
		scorm = pipwerks.SCORM,
		trace = pipwerks.UTILS.trace,
        code = 0;

    if(API){

		switch(scorm.version){
			case "1.2" : code = parseInt(API.LMSGetLastError(), 10); break;
			case "2004": code = parseInt(API.GetLastError(), 10); break;
		}
		     
    } else {
     
        trace("SCORM.debug.getCode failed: API is null.");

    }
     
    return code;
    
};


/* -------------------------------------------------------------------------
   pipwerks.SCORM.debug.getInfo()
   "Used by a SCO to request the textual description for the error code
   specified by the value of [errorCode]."

   Parameters: errorCode (integer).  
   Returns:    String.
----------------------------------------------------------------------------- */

pipwerks.SCORM.debug.getInfo = function(errorCode){
     
    var API = pipwerks.SCORM.API.getHandle(),
		scorm = pipwerks.SCORM,
		trace = pipwerks.UTILS.trace,
        result = "";
     
		
    if(API){
          
		switch(scorm.version){
			case "1.2" : result = API.LMSGetErrorString(errorCode.toString()); break;
			case "2004": result = API.GetErrorString(errorCode.toString()); break;
		}
		
    } else {
     
        trace("SCORM.debug.getInfo failed: API is null.");

    }
     
    return String(result);

};


/* -------------------------------------------------------------------------
   pipwerks.SCORM.debug.getDiagnosticInfo
   "Exists for LMS specific use. It allows the LMS to define additional
   diagnostic information through the API Instance."

   Parameters: errorCode (integer).  
   Returns:    String (Additional diagnostic information about the given error code).
---------------------------------------------------------------------------- */

pipwerks.SCORM.debug.getDiagnosticInfo = function(errorCode){

    var API = pipwerks.SCORM.API.getHandle(),
		scorm = pipwerks.SCORM,
		trace = pipwerks.UTILS.trace,
        result = "";
		
    if(API){

		switch(scorm.version){
			case "1.2" : result = API.LMSGetDiagnostic(errorCode); break;
			case "2004": result = API.GetDiagnostic(errorCode); break;
		}
		
    } else {
     
        trace("SCORM.debug.getDiagnosticInfo failed: API is null.");

    }

    return String(result);

};


// ------------------------------------------------------------------------- //
// --- Shortcuts! ---------------------------------------------------------- //
// ------------------------------------------------------------------------- //

// Because nobody likes typing verbose code.

pipwerks.SCORM.init = pipwerks.SCORM.connection.initialize;
pipwerks.SCORM.get  = pipwerks.SCORM.data.get;
pipwerks.SCORM.set  = pipwerks.SCORM.data.set;
pipwerks.SCORM.save = pipwerks.SCORM.data.save;
pipwerks.SCORM.quit = pipwerks.SCORM.connection.terminate;



// ------------------------------------------------------------------------- //
// --- pipwerks.UTILS functions -------------------------------------------- //
// ------------------------------------------------------------------------- //


/* -------------------------------------------------------------------------
   pipwerks.UTILS.StringToBoolean()
   Converts 'boolean strings' into actual valid booleans.
   
   (Most values returned from the API are the strings "true" and "false".)

   Parameters: String
   Returns:    Boolean
---------------------------------------------------------------------------- */

pipwerks.UTILS.StringToBoolean = function(string){
     switch(string.toLowerCase()) {
          case "true": case "yes": case "1": return true;
          case "false": case "no": case "0": case null: return false; 
          default: return Boolean(string);
     }     
};



/* -------------------------------------------------------------------------
   pipwerks.UTILS.trace()
   Displays error messages when in debug mode.

   Parameters: msg (string)  
   Return:     None
---------------------------------------------------------------------------- */

pipwerks.UTILS.trace = function(msg){

     if(pipwerks.debug.isActive){
     
		//Firefox users can use the 'Firebug' extension's console.
		if(window.console && window.console.firebug){
			console.log(msg);
		} else {
			//alert(msg);
		}
		
     }
};

 class Scorm {
    constructor(debug) {
        this.debugger = debug, 0 == this.debugger && (this.scorm = pipwerks.SCORM, this.scorm.version = "1.2", console.log("Inicialización."), this.callSucceeded = this.scorm.init())
    }
  
    
    complete() {
        this.callSucceeded = this.scorm.set("cmi.core.lesson_status", "completed");
    }
    incomplete() {
        this.callSucceeded = this.scorm.set("cmi.core.lesson_status", "incomplete");
    }
    end() {
        var callSucceeded;
        return this.scorm.quit()
    }
    get(item) {
        this.scorm.get(item)
    }
    set(item, save) {
        this.scorm.set(item, save)
    }
    info_user() {
        var dates;
        return {
            name: 0 == this.debugger ? String(this.scorm.get("cmi.core.student_name")) : "Test User",
            id: 0 == this.debugger ? String(this.scorm.get("cmi.core.student_id")) : "Test identification"
        }
    }
    reset_commit() {
        this.set_commit("cmi.core.lesson_location", null)
    }
    set_time(save) {
        0 == this.debugger ? this.scorm.set("cmi.core.session_time", save) : localStorage.setItem("cmi.core.session_time", save)
    }
    set_location(save) {
        0 == this.debugger ? this.scorm.set("cmi.core.lesson_location", save) : localStorage.setItem("cmi.core.lesson_location", save)
    }
    get_location() {
        var commit = 0 == this.debugger ? String(this.scorm.get("cmi.core.lesson_location")) : String(localStorage.getItem("cmi.core.lesson_location"));
        return "" == commit || null == commit ? null : JSON.parse(commit)
    }
    set_entry(save) {
        0 == this.debugger ? this.scorm.set("cmi.interactions.0.time", save) : localStorage.setItem("cmi.interactions.0.time", save)
    }
    set_finish(save) {
        0 == this.debugger ? this.scorm.set("cmi.interactions.1.time", save) : localStorage.setItem("cmi.interactions.1.time", save)
    }
    set_commit(save) {
        0 == this.debugger ? (save = JSON.stringify(save), this.scorm.set("cmi.comments", save)) : (save = JSON.stringify(save), localStorage.setItem("cmi.comments", save))
    }
    get_commit() {
        var commit = 0 == this.debugger ? String(this.scorm.get("cmi.comments")) : String(localStorage.getItem("cmi.comments"));
        return "" == commit || null == commit ? null : commit
    }
    set_score(note) {
        0 == this.debugger ? (this.set("cmi.core.score.raw", String(note)), this.set("Write", String(note / 100)), this.complete(), this.end()) : (localStorage.setItem("cmi.score.raw", String(note)), localStorage.setItem("Write", String(note / 100)))
    }
    get_score(nota) {
        if ("raw" == nota) return 0 == this.debugger ? this.scorm.get("cmi.core.score.raw") : localStorage.getItem("cmi.core.score.raw")
    }
}



window.addEventListener('load', function() {
 
   // stage2.stop();
   const hojasDeEstilo = document.styleSheets;
const urls = [];
for (let i = 0; i < hojasDeEstilo.length; i++) {
  const reglas = hojasDeEstilo[i].cssRules;
  for (let j = 0; j < reglas.length; j++) {
    if (reglas[j].style && reglas[j].style.backgroundImage) {
      const url = reglas[j].style.backgroundImage.replace(/url\(['"]?([^'"]+)['"]?\)/i, '$1');
      urls.push(url);
    }
  }
}

// Preload de las imágenes
let contador = 0;
for (let i = 0; i < urls.length; i++) {
  const imagen = new Image();
  imagen.src = urls[i];
  imagen.addEventListener('load', function() {
    contador++;
    if (contador === urls.length) {
      // Todas las imágenes han cargado
      console.log('Todas las imágenes de los archivos CSS han cargado.');
      main();
    }
  });
}
  
});
var audios = [
    {
    url: "./sounds/click.mp3",
    name: "clic"
    },
    {
        url: "./sounds/Slide2.mp3",
        name: "songS2"
    },
    {
        url: "./sounds/Slide3.mp3",
        name: "songS3"
    },
    {
        url: "./sounds/Slide7.mp3",
        name: "songS7"
    },
    {
        url: "./sounds/Slide8.mp3",
        name: "songS8"
    },
    {
        url: "./sounds/Slide12.mp3",
        name: "songS12"
    },
    {
        url: "./sounds/Slide13.mp3",
        name: "songS13"
    },
    {
        url: "./sounds/Slide17.mp3",
        name: "songS17"
    },
    {
        url: "./sounds/Slide18.mp3",
        name: "songS18"
    },
    {
        url: "./sounds/Slide22.mp3",
        name: "songS22"
    },
];
ivo.info({
    title: "Universidad de Cindinamarca",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var stage1;
var stage2;
var stage3;
var stage4;
var stage5;
var stage6;
var stage6_1;
var stage6_2;
var stage7;
var stage8;
var stage9;
var stage10;
var stage11;
var stage12;
var stage13;
var stage14;
var stage15;
var stage16;
var stage17;
var stage18;
var stage19;
var stage20;
var stage21;
var stage22;
var stage23;
var respuestas=["none","none","none","none","none","none","none","none","none","none","none","none"];
var Scorm_mx;
function main() {
    var t = null;
    var udec = ivo.structure({
        created: function () {
            t = this;
            t.getDevice();
            
            //precarga audios//
            var onComplete = function () {
                console.log("audios cargados");
                t.animations();
                t.events();
                
                stage1.play();
                ivo("#preload").hide();
                
                ivo("#app").show();
            };
            ivo.load_audio(audios,onComplete );
        },
        methods: {
            getDevice:function(){
                this.movil = false;
                let windowWidth = window.innerWidth;
                if (windowWidth < 700) {
                    this.movil = true;
                    
                }
                if('ontouchstart' in window || navigator.maxTouchPoints) {
                    this.movil = true;
                }
            },
            registerNote:function () {
                //recorremos respuestas y verificamos que todas sean difentees de note
                var count = 0;
                var puntaje=0;
                for (let i = 0; i < respuestas.length; i++) {
                    if (respuestas[i] != "none") {
                        count++;
                    }
                }
                if (count == respuestas.length) {
                    //ya contesto todo recorremos nuevamente para revisar los aciertos
                    for (let j = 0; j < respuestas.length; j++) {
                        if (respuestas[j] == true) {
                            puntaje++;
                        }
                    }
                    //debemos enviar nota
                    let scale = 50 / 12;
                    let nota = puntaje * scale;

                    Scorm_mx = new Scorm(false);
                    console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id + " Nota: " + nota);
                    Scorm_mx.set_score(nota);

                }else{
                    //le faltan preguntas
                    console.warn("le faltan preguntas");
                }

            },
            events: function () {
                var _this = this;
                ivo("#stage1_btn_start").on("click", function () {
                    stage1.timeScale(3).reverse();
                    stage2.timeScale(1).play();
                    ivo.play("clic");
                });
                //segundo slide textos
                const showinfo = (n) => {
                    //vamos ocultar los divs hijos de #stage2_textos
                    var divs = document.querySelectorAll('#stage2_textos > div');
                    divs.forEach((div) => {
                        div.style.display = 'none';
                    });
                    //vamos a mostrar n por medio de nth-child
                    var div = document.querySelector('#stage2_textos > div:nth-child(' + n + ')');
                    div.style.display = 'block';
                };
                // Stage2 
                showinfo(1);
                
                ivo("#stage2_btn_1").on("click", function () {
                    showinfo(1);
                    ivo.play("clic");
                });
                ivo("#stage2_btn_2").on("click", function () {
                    showinfo(2);
                    ivo.play("clic");
                });
                ivo("#stage2_btn_3").on("click", function () {
                    showinfo(3);
                    ivo.play("clic");
                });
                ivo("#stage2_btn_4").on("click", function () {
                    stage2.timeScale(3).reverse();
                    stage3.timeScale(1).play();
                    ivo.play("clic");
                });

                // Stage3
                ivo("#stage3_btn_ingresar").on("click", function () {
                    stage3.timeScale(3).reverse();
                    stage4.timeScale(1).play();
                    ivo.play("clic");
                });

                // Stage4
                ivo("#stage4_btn_1").on("click", function () {
                    stage4.timeScale(3).reverse();
                    stage5.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#stage4_btn_2,#stage4_btn_3,#stage4_btn_4,#stage4_btn_5").css("cursor","no-drop");

                // Stage5
                ivo("#stage5_btn_1").on("click", function () {
                    ivo("#stage5_texto").show();
                    ivo.play("clic");
                });
                ivo("#stage5_btn_2").on("click", function () {
                    ivo.play("songS3");
                });
                ivo("#stage5_btn_atras").on("click", function () {
                    stage5.timeScale(3).reverse();
                    stage4.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#stage5_btn_siguiente").on("click", function () {
                    stage5.timeScale(3).reverse();
                    stage6.timeScale(1).play();
                    ivo.play("clic");
                });
                
                ivo("#stage5_btn_1").on("click", function () {
                    // ivo.play("clic");
                });

                // Stage6
                ivo("#stage6_btn_atras").on("click", function () {
                    stage6.timeScale(3).reverse();
                    stage5.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#stage6_2_btn_atras").on("click", function () {
                    stage6_2.timeScale(3).reverse();
                    stage6_1.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#stage6_1_btn_atras").on("click", function () {
                    stage6_1.timeScale(3).reverse();
                    stage6.timeScale(1).play();
                    ivo.play("clic");
                });

                ivo("#stage6_btn_siguiente").on("click", function () {
                    stage6.timeScale(3).reverse();
                    stage6_1.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#stage6_1_btn_siguiente").on("click", function () {
                    stage6.timeScale(3).reverse();
                    stage6_2.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#stage6_2_btn_siguiente").on("click", function () {
                    stage6.timeScale(3).reverse();
                    stage7.timeScale(1).play();
                    ivo.play("clic");
                });
                
                // Respuestas a las preguntas
                const showAnswer = (n,stage) => {
                    
                    for (let i = 1; i <= 4; i++) {
                        let img = document.querySelector(`#${stage}${i}`);
                        img.style.visibility = 'hidden';
                    }

                    //vamos a mostrar n por medio de nth-child
                    let img = document.querySelector(`#${stage}${n}`);
                    // img.style.remove()
                    console.log(`#stage6_box_${n}`);
                    img.style.visibility = 'visible';
                };

                // Stage6
                ivo("#stage6_btn_fortaleza").on("click", function () {
                    if (respuestas[0] !="none") return false;
                    showAnswer(1,"stage6_box_");
                    ivo.play("clic");
                    respuestas[0]=false;
                    _this.registerNote();
                });
                ivo("#stage6_btn_oportunidad").on("click", function () {
                    if (respuestas[0] !="none") return false;
                    showAnswer(2,"stage6_box_");
                    ivo.play("clic");
                    respuestas[0]=false;
                    _this.registerNote();
                });
                ivo("#stage6_btn_debilidad").on("click", function () {
                    if (respuestas[0] !="none") return false;
                    showAnswer(3,"stage6_box_");
                    ivo.play("clic");
                    respuestas[0]=false;
                    _this.registerNote();
                });
                ivo("#stage6_btn_amenaza").on("click", function () {
                    if (respuestas[0] !="none") return false;
                    showAnswer(4,"stage6_box_");
                    ivo.play("clic");
                    respuestas[0]=true;
                    _this.registerNote();
                });

                ivo("#stage6_1_btn_A").on("click", function () {
                    if (respuestas[1] !="none") return false;
                    showAnswer(1,"stage6_1_box_");
                    ivo.play("clic");
                    respuestas[1]=true;
                    _this.registerNote();
                });
                ivo("#stage6_1_btn_B").on("click", function () {
                    if (respuestas[1] !="none") return false;
                    showAnswer(2,"stage6_1_box_");
                    ivo.play("clic");
                    respuestas[1]=false;
                    _this.registerNote();
                });
                ivo("#stage6_1_btn_C").on("click", function () {
                    if (respuestas[1] !="none") return false;
                    showAnswer(3,"stage6_1_box_");
                    ivo.play("clic");
                    respuestas[1]=false;
                    _this.registerNote();
                });
                ivo("#stage6_1_btn_D").on("click", function () {
                    if (respuestas[1] !="none") return false;
                    showAnswer(4,"stage6_1_box_");
                    ivo.play("clic");
                    respuestas[1]=false;
                    _this.registerNote();
                });

                ivo("#stage6_2_btn_A").on("click", function () {
                    if (respuestas[2] !="none") return false;
                    showAnswer(1,"stage6_2_box_");
                    ivo.play("clic");
                    respuestas[2]=false;
                    _this.registerNote();
                });
                ivo("#stage6_2_btn_B").on("click", function () {
                    if (respuestas[2] !="none") return false;
                    showAnswer(2,"stage6_2_box_");
                    ivo.play("clic");
                    respuestas[2]=true;
                    _this.registerNote();
                });
                ivo("#stage6_2_btn_C").on("click", function () {
                    if (respuestas[2] !="none") return false;
                    showAnswer(3,"stage6_2_box_");
                    ivo.play("clic");
                    respuestas[2]=false;
                    _this.registerNote();
                });
                ivo("#stage6_2_btn_D").on("click", function () {
                    if (respuestas[2] !="none") return false;
                    showAnswer(4,"stage6_2_box_");
                    ivo.play("clic");
                    respuestas[2]=false;
                    _this.registerNote();
                });

                // Stage7
                ivo("#stage7_btn_menu").on("click", function () {
                    stage7.timeScale(3).reverse();
                    stage8.timeScale(1).play();
                    ivo.play("clic");
                });                

                // Stage8
                ivo("#stage8_btn_3").on("click", function () {
                    stage8.timeScale(3).reverse();
                    stage9.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#stage8_btn_1,#stage8_btn_4,#stage8_btn_2,#stage8_btn_5").css("cursor","no-drop");

                // Stage9
                ivo("#stage9_btn_atras").on("click", function () {
                    stage9.timeScale(3).reverse();
                    stage8.timeScale(1).play();
                    ivo.play("clic");
                });

                ivo("#stage9_btn_siguiente").on("click", function () {
                    stage9.timeScale(3).reverse();
                    stage10.timeScale(1).play();
                    ivo.play("clic");
                });
                
             
                
                ivo("#stage9_btn_coercitivo").on("click", function () {
                    if (respuestas[3] !="none") return false;
                    showAnswer(1,"stage9_box_");
                    ivo.play("clic");
                    respuestas[3]=false;
                    _this.registerNote();
                });
                ivo("#stage9_btn_democratico").on("click", function () {
                    if (respuestas[3] !="none") return false;
                    showAnswer(2,"stage9_box_");
                    ivo.play("clic");
                    respuestas[3]=false;
                    _this.registerNote();
                });
                ivo("#stage9_btn_autoritario").on("click", function () {
                    if (respuestas[3] !="none") return false;
                    showAnswer(3,"stage9_box_");
                    ivo.play("clic");
                    respuestas[3]=true;
                    _this.registerNote();
                });
                ivo("#stage9_btn_participativo").on("click", function () {
                    if (respuestas[3] !="none") return false;
                    showAnswer(4,"stage9_box_");
                    ivo.play("clic");
                    respuestas[3]=false;
                    _this.registerNote();                    
                });

                // Stage10
                ivo("#stage10_btn_atras").on("click", function () {
                    stage10.timeScale(3).reverse();
                    stage9.timeScale(1).play();
                    ivo.play("clic");
                });

                ivo("#stage10_btn_siguiente").on("click", function () {
                    stage10.timeScale(3).reverse();
                    stage11.timeScale(1).play();
                    ivo.play("clic");
                });
                
                
                
                ivo("#stage10_btn_flexibilidad").on("click", function () {
                    if (respuestas[4] !="none") return false;
                    showAnswer(1,"stage10_box_");
                    ivo.play("clic");
                    respuestas[4]=true;
                    _this.registerNote();
                });
                ivo("#stage10_btn_dominio").on("click", function () {
                    if (respuestas[4] !="none") return false;
                    showAnswer(2,"stage10_box_");
                    ivo.play("clic");
                    respuestas[4]=false;
                    _this.registerNote();
                });
                ivo("#stage10_btn_portavoz").on("click", function () {
                    if (respuestas[4] !="none") return false;
                    showAnswer(3,"stage10_box_");
                    ivo.play("clic");
                    respuestas[4]=false;
                    _this.registerNote();
                });
                ivo("#stage10_btn_locus").on("click", function () {
                    if (respuestas[4] !="none") return false;
                    showAnswer(4,"stage10_box_");
                    ivo.play("clic");
                    respuestas[4]=false;
                    _this.registerNote();
                });

                // Stage11
                ivo("#stage11_btn_atras").on("click", function () {
                    stage11.timeScale(3).reverse();
                    stage10.timeScale(1).play();
                    ivo.play("clic");
                });

                ivo("#stage11_btn_siguiente").on("click", function () {
                    stage11.timeScale(3).reverse();
                    stage12.timeScale(1).play();
                    ivo.play("clic");
                });
                
                // Respuestas a las preguntas
                
                
                ivo("#stage11_btn_clima").on("click", function () {
                    if (respuestas[5] !="none") return false;
                    showAnswer(1,"stage11_box_");
                    ivo.play("clic");
                    respuestas[5]=true;
                    _this.registerNote();
                });
                ivo("#stage11_btn_estrategias").on("click", function () {
                    if (respuestas[5] !="none") return false;
                    showAnswer(2,"stage11_box_");
                    ivo.play("clic");
                    respuestas[5]=false;
                    _this.registerNote();
                });
                ivo("#stage11_btn_escuchar").on("click", function () {
                    if (respuestas[5] !="none") return false;
                    showAnswer(3,"stage11_box_");
                    ivo.play("clic");
                    respuestas[5]=false;
                    _this.registerNote();
                });
                ivo("#stage11_btn_fortalezas").on("click", function () {
                    if (respuestas[5] !="none") return false;
                    showAnswer(4,"stage11_box_");
                    ivo.play("clic");
                    respuestas[5]=false;
                    _this.registerNote();
                });

                // Stage12
                ivo("#stage12_btn_menu").on("click", function () {
                    stage12.timeScale(3).reverse();
                    stage13.timeScale(1).play();
                    ivo.play("clic");
                }); 

                // Stage13
                ivo("#stage13_btn_2").on("click", function () {
                    stage13.timeScale(3).reverse();
                    stage14.timeScale(1).play();
                    ivo.play("clic");
                }); 
                ivo("#stage13_btn_1,#stage13_btn_3,#stage13_btn_4,#stage13_btn_5").css("cursor","no-drop");
                // Stage14
                ivo("#stage14_btn_atras").on("click", function () {
                    stage14.timeScale(3).reverse();
                    stage13.timeScale(1).play();
                    ivo.play("clic");
                });

                ivo("#stage14_btn_siguiente").on("click", function () {
                    stage14.timeScale(3).reverse();
                    stage15.timeScale(1).play();
                    ivo.play("clic");
                });
                
                ivo("#stage14_btn_marketing").on("click", function () {
                    if (respuestas[6] !="none") return false;
                    showAnswer(1,"stage14_box_");
                    ivo.play("clic");
                    respuestas[6]=false;
                    _this.registerNote();
                });
                ivo("#stage14_btn_aranceles").on("click", function () {
                    if (respuestas[6] !="none") return false;
                    showAnswer(2,"stage14_box_");
                    ivo.play("clic");
                    respuestas[6]=false;
                    _this.registerNote();
                });
                ivo("#stage14_btn_sucursales").on("click", function () {
                    if (respuestas[6] !="none") return false;
                    showAnswer(3,"stage14_box_");
                    ivo.play("clic");
                    respuestas[6]=false;
                    _this.registerNote();
                });
                ivo("#stage14_btn_indicadores").on("click", function () {
                    if (respuestas[6] !="none") return false;
                    showAnswer(4,"stage14_box_");
                    ivo.play("clic");
                    respuestas[6]=false;
                    _this.registerNote();
                });

                // Stage15
                ivo("#stage15_btn_atras").on("click", function () {
                    stage15.timeScale(3).reverse();
                    stage14.timeScale(1).play();
                    ivo.play("clic");
                });

                ivo("#stage15_btn_siguiente").on("click", function () {
                    stage15.timeScale(3).reverse();
                    stage16.timeScale(1).play();
                    ivo.play("clic");
                });
                
              
                
                
                ivo("#stage15_btn_creatividad").on("click", function () {
                    if (respuestas[7] !="none") return false;
                    showAnswer(1,"stage15_box_");
                    ivo.play("clic");
                    respuestas[7]=false;
                    _this.registerNote();
                });
                ivo("#stage15_btn_resolucion").on("click", function () {
                    if (respuestas[7] !="none") return false;
                    showAnswer(2,"stage15_box_");
                    ivo.play("clic");
                    respuestas[7]=true;
                    _this.registerNote();
                });
                ivo("#stage15_btn_gerencia").on("click", function () {
                    if (respuestas[7] !="none") return false;
                    showAnswer(3,"stage15_box_");
                    ivo.play("clic");
                    respuestas[7]=false;
                    _this.registerNote();
                });
                ivo("#stage15_btn_toma").on("click", function () {
                    if (respuestas[7] !="none") return false;
                    showAnswer(4,"stage15_box_");
                    ivo.play("clic");
                    respuestas[7]=false;
                    _this.registerNote();
                });

                // Stage16
                ivo("#stage16_btn_atras").on("click", function () {
                    stage16.timeScale(3).reverse();
                    stage15.timeScale(1).play();
                    ivo.play("clic");
                });

                ivo("#stage16_btn_siguiente").on("click", function () {
                    stage16.timeScale(3).reverse();
                    stage17.timeScale(1).play();
                    ivo.play("clic");
                });
                
                // Respuestas a las preguntas
                
                ivo("#stage16_btn_proveedor").on("click", function () {
                    if (respuestas[8] !="none") return false;
                    showAnswer(1,"stage16_box_");
                    ivo.play("clic");
                    respuestas[8]=false;
                    _this.registerNote();
                });
                ivo("#stage16_btn_directivo").on("click", function () {
                    if (respuestas[8] !="none") return false;
                    showAnswer(2,"stage16_box_");
                    ivo.play("clic");
                    respuestas[8]=true;
                    _this.registerNote();
                });
                ivo("#stage16_btn_lider").on("click", function () {
                    if (respuestas[8] !="none") return false;
                    showAnswer(3,"stage16_box_");
                    ivo.play("clic");
                    respuestas[8]=false;
                    _this.registerNote();
                });
                ivo("#stage16_btn_colaborador").on("click", function () {
                    if (respuestas[8] !="none") return false;
                    showAnswer(4,"stage16_box_");
                    ivo.play("clic");
                    respuestas[8]=false;
                    _this.registerNote();
                });

                // Stage17
                ivo("#stage17_btn_menu").on("click", function () {
                    stage17.timeScale(3).reverse();
                    stage18.timeScale(1).play();
                    ivo.play("clic");
                }); 

                // Stage18
                ivo("#stage18_btn_4").on("click", function () {
                    stage18.timeScale(3).reverse();
                    stage19.timeScale(1).play();
                    ivo.play("clic");
                }); 
                ivo("#stage18_btn_1,#stage18_btn_2,#stage18_btn_3,#stage18_btn_5").css("cursor","no-drop");

                // Stage19
                ivo("#stage19_btn_atras").on("click", function () {
                    stage19.timeScale(3).reverse();
                    stage18.timeScale(1).play();
                    ivo.play("clic");
                });

                ivo("#stage19_btn_siguiente").on("click", function () {
                    stage19.timeScale(3).reverse();
                    stage20.timeScale(1).play();
                    ivo.play("clic");
                });
                
                
                
                ivo("#stage19_btn_especifica").on("click", function () {
                    if (respuestas[9] !="none") return false;
                    showAnswer(1,"stage19_box_");
                    ivo.play("clic");
                    respuestas[9]=false;
                    _this.registerNote();
                });
                ivo("#stage19_btn_personal").on("click", function () {
                    if (respuestas[9] !="none") return false;
                    showAnswer(2,"stage19_box_");
                    ivo.play("clic");
                    respuestas[9]=false;
                    _this.registerNote();
                });
                ivo("#stage19_btn_social").on("click", function () {
                    if (respuestas[9] !="none") return false;
                    showAnswer(3,"stage19_box_");
                    ivo.play("clic");
                    respuestas[9]=false;
                    _this.registerNote();
                });
                ivo("#stage19_btn_profesional").on("click", function () {
                    if (respuestas[9] !="none") return false;
                    showAnswer(4,"stage19_box_");
                    ivo.play("clic");
                    respuestas[9]=true;
                    _this.registerNote();
                });

                // Stage20
                ivo("#stage20_btn_atras").on("click", function () {
                    stage20.timeScale(3).reverse();
                    stage19.timeScale(1).play();
                    ivo.play("clic");
                });

                ivo("#stage20_btn_siguiente").on("click", function () {
                    stage20.timeScale(3).reverse();
                    stage21.timeScale(1).play();
                    ivo.play("clic");
                });
                
              
                
                ivo("#stage20_btn_administrativas").on("click", function () {
                    if (respuestas[10] !="none") return false;
                    showAnswer(1,"stage20_box_");
                    ivo.play("clic");
                    respuestas[10]=false;
                    _this.registerNote();
                });
                ivo("#stage20_btn_organizacional").on("click", function () {
                    if (respuestas[10] !="none") return false;
                    showAnswer(2,"stage20_box_");
                    ivo.play("clic");
                    respuestas[10]=false;
                    _this.registerNote();
                });
                ivo("#stage20_btn_proceso").on("click", function () {
                    if (respuestas[10] !="none") return false;
                    showAnswer(3,"stage20_box_");
                    ivo.play("clic");
                    respuestas[10]=false;
                    _this.registerNote();
                });
                ivo("#stage20_btn_emocional").on("click", function () {
                    if (respuestas[10] !="none") return false;
                    showAnswer(4,"stage20_box_");
                    ivo.play("clic");
                    respuestas[10]=true;
                    _this.registerNote();
                });

                // Stage21
                ivo("#stage21_btn_atras").on("click", function () {
                    stage21.timeScale(3).reverse();
                    stage20.timeScale(1).play();
                    ivo.play("clic");
                });

                ivo("#stage21_btn_siguiente").on("click", function () {
                    stage21.timeScale(3).reverse();
                    stage22.timeScale(1).play();
                    ivo.play("clic");
                });
                
               
                
                ivo("#stage21_btn_compartir").on("click", function () {
                    if (respuestas[11] !="none") return false;
                    showAnswer(1,"stage21_box_");
                    ivo.play("clic");
                    respuestas[11]=false;
                    _this.registerNote();
                });
                ivo("#stage21_btn_hacer").on("click", function () {
                    if (respuestas[11] !="none") return false;
                    showAnswer(2,"stage21_box_");
                    ivo.play("clic");
                    respuestas[11]=false;
                    _this.registerNote();
                });
                ivo("#stage21_btn_mantener").on("click", function () {
                    if (respuestas[11] !="none") return false;
                    showAnswer(3,"stage21_box_");
                    ivo.play("clic");
                    respuestas[11]=false;
                    _this.registerNote();
                });
                ivo("#stage21_btn_participar").on("click", function () {
                    if (respuestas[11] !="none") return false;
                    showAnswer(4,"stage21_box_");
                    ivo.play("clic");
                    respuestas[11]=true;
                    _this.registerNote();
                });

                // Stage22
                ivo("#stage22_btn_grafico").on("click", function () {
                    stage22.timeScale(3).reverse();
                    stage23.timeScale(1).play();
                    ivo.play("clic");
                });

                /*               
                
                ivo("#btn_credits").on("click", function () {
                    stage3.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo("#credits_close").on("click", function () {
                    stage3.timeScale(3).reverse();
                    ivo.play("clic");
                });
                let rules = function(rule){
                    console.log("rules"+rule);
                    
                };
                let onMove=function(page){
                    console.log("onMove"+page);
                    
                    ivo.play("clic");
                };
                let onFinish=function(){
                }
            
                var slider=ivo("#slider").slider({
                    slides:'.slider',
                    btn_next:"#btn_next",
                    btn_back:"#btn_back",
                    rules:rules,
                    onMove:onMove,
                    onFinish:onFinish
                });*/
            },            
            animations: function () {
                let _this=this;
                stage1 = new TimelineMax();
                stage2 = new TimelineMax();
                stage3 = new TimelineMax();
                stage4 = new TimelineMax({onComplete: function () {
                    if(!_this.movil){
                    ivo.play("songS2");
                    }
                }});
                stage5 = new TimelineMax();
                stage6 = new TimelineMax();
                stage6_1 = new TimelineMax();
                stage6_2 = new TimelineMax();
                stage7 = new TimelineMax({
                    onComplete: function () {
                        if(!_this.movil){
                            ivo.play("songS7");
                        }
                        
                    }
                });
                stage8 = new TimelineMax({
                    onComplete: function () {
                        if(!_this.movil){
                        ivo.play("songS8");
                        }
                    }
                });
                stage9 = new TimelineMax();
                stage10 = new TimelineMax();
                stage11 = new TimelineMax();
                stage12 = new TimelineMax({
                    onComplete: function () {
                        if(!_this.movil){
                        ivo.play("songS12");
                        }
                    }
                });
                stage13 = new TimelineMax({
                    onComplete: function () {
                        if(!_this.movil){
                        ivo.play("songS13");
                        }
                    }
                });
                stage14 = new TimelineMax();
                stage15 = new TimelineMax();
                stage16 = new TimelineMax();
                stage17 = new TimelineMax({
                    onComplete: function () {
                        if(!_this.movil){
                        ivo.play("songS17");
                        }
                    }
                });
                stage18 = new TimelineMax({
                    onComplete: function () {
                        if(!_this.movil){
                        ivo.play("songS18");
                        }
                    }
                });
                stage19 = new TimelineMax();
                stage20 = new TimelineMax();
                stage21 = new TimelineMax();
                stage22 = new TimelineMax({
                    onComplete: function () {
                        if(!_this.movil){
                        ivo.play("songS22");
                        }
                    }
                });
                stage23 = new TimelineMax();
                stage1.append(TweenMax.from("#stage1", .8, {y: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from("#stage1_title", .8, {x: -1300, opacity: 0}), 0);
                stage1.append(TweenMax.from("#stage1_btn_start", .8, {x: 1300,rotation:900, opacity: 0}), 0);
                stage1.stop();

                stage2.append(TweenMax.from("#stage2", .8, {y: 1300, opacity: 0}), 0);
                stage2.append(TweenMax.staggerFrom(".btn1", .8, {x: 1300, opacity: 0}, 0.2), 0);
                //  stage2.append(TweenMax.from("#stage2_title", .8, {x: -1300, opacity: 0}), 0);
                stage2.stop();

                stage3.append(TweenMax.from("#stage3", .8, {y: 1300, opacity: 0}, 0.2), 0);
                stage3.append(TweenMax.from("#stage3_titulo", .8, {y: -130, opacity: 0}, 0.2), 0);
                stage3.append(TweenMax.from("#stage3_btn_ingresar", .8, {y: 130, opacity: 0}, 0.2), 0);
                stage3.stop();

                stage4.append(TweenMax.from("#stage4", .8, {y: 1300, opacity: 0}), 0);
                stage4.append(TweenMax.staggerFrom(".stage4_btns", .8, {y: 130, opacity: 0}, 0.2), 0);
                stage4.append(TweenMax.from("#stage4_texto", .8, {y: -130, opacity: 0}, 0.2), 0);
                stage4.stop();

                stage5.append(TweenMax.from("#stage5", .8, {y: 1300, opacity: 0}), 0);
                stage5.append(TweenMax.staggerFrom(".stage5_btns", .8, {y: 130, opacity: 0}, 0.2), 0);
                stage5.append(TweenMax.from("#stage5_texto", .8, {y: -130, opacity: 0}, 0.2), 0);
                stage5.stop();

                stage6.append(TweenMax.from("#stage6", .8, {y: 1300, opacity: 0}), 0);
                stage6.append(TweenMax.from("#stage6_texto", .8, {y: -130, opacity: 0}, 0.2), 0);
                stage6.append(TweenMax.staggerFrom(".stage6_btns", .8, {x: 130, opacity: 0}, 0.2), 0);
                stage6.stop();

                stage6_1.append(TweenMax.from("#stage6_1", .8, {y: 1300, opacity: 0}), 0);
                stage6_1.append(TweenMax.from("#stage6_1_texto", .8, {y: -130, opacity: 0}, 0.2), 0);
                stage6_1.append(TweenMax.staggerFrom(".stage6_1_btns", .8, {x: 130, opacity: 0}, 0.2), 0);
                stage6_1.stop();

                stage6_2.append(TweenMax.from("#stage6_2", .8, {y: 1300, opacity: 0}), 0);
                stage6_2.append(TweenMax.from("#stage6_2_texto", .8, {y: -130, opacity: 0}, 0.2), 0);
                stage6_2.append(TweenMax.staggerFrom(".stage6_2_btns", .8, {x: 130, opacity: 0}, 0.2), 0);
                stage6_2.stop();

                stage7.append(TweenMax.from("#stage7", .8, {y: 1300, opacity: 0}, 0.2), 0);
                stage7.append(TweenMax.from("#stage7_texto_1", .8, {y: -200, opacity: 0}, 0.2), 0);
                stage7.append(TweenMax.from("#stage7_texto_2", .8, {y: 200, opacity: 0}, 0.2), 0);
                stage7.append(TweenMax.from("#stage7_btn_menu", .8, {x: 200, opacity: 0}, 0.2), 0);
                stage7.stop();

                stage8.append(TweenMax.from("#stage8", .8, {y: 1300, opacity: 0}, 0.2), 0);
                stage8.append(TweenMax.staggerFrom(".stage8_btns", .8, {y: 130, opacity: 0}, 0.2), 0);
                stage8.append(TweenMax.from("#stage8_texto_1", .8, {y: -10, opacity: 0}, 0.2), 0);
                stage8.append(TweenMax.from("#stage8_texto_2", .8, {y: -10, opacity: 0}, 0.2), 0);
                stage8.stop();

                stage9.append(TweenMax.from("#stage9", .8, {y: 1300, opacity: 0}, 0.2), 0);
                stage9.append(TweenMax.from("#stage9_texto_1", .8, {y: -10, opacity: 0}, 0.2), 0);               
                stage9.append(TweenMax.from("#stage9_texto_2", .8, {y: 10, opacity: 0}, 0.2), 0);
                stage9.append(TweenMax.staggerFrom(".stage9_btns", .8, {x: 130, opacity: 0}, 0.2), 0);
                stage9.stop();

                stage10.append(TweenMax.from("#stage10", .8, {y: 1300, opacity: 0}, 0.2), 0);
                stage10.append(TweenMax.from("#stage10_texto_1", .8, {y: -10, opacity: 0}, 0.2), 0);  
                stage10.append(TweenMax.staggerFrom(".stage10_btns", .8, {x: 130, opacity: 0}, 0.2), 0);
                stage10.stop();

                stage11.append(TweenMax.from("#stage11", .8, {y: 1300, opacity: 0}), 0);
                stage11.append(TweenMax.from("#stage11_texto_1", .8, {y: -10, opacity: 0}, 0.2), 0);  
                stage11.append(TweenMax.staggerFrom(".stage11_btns", .8, {x: 130, opacity: 0}, 0.2), 0);
                stage11.stop();

                stage12.append(TweenMax.from("#stage12", .8, {y: 1300, opacity: 0}), 0);
                stage12.append(TweenMax.from("#stage12_texto_1", .8, {y: -10, opacity: 0}, 0.2), 0); 
                stage12.append(TweenMax.from("#stage12_btn_menu", .8, {x: 200, opacity: 0}, 0.2), 0);
                stage12.stop();

                stage13.append(TweenMax.from("#stage13", .8, {y: 1300, opacity: 0}), 0);
                stage13.append(TweenMax.staggerFrom(".stage13_btns", .8, {y: 130, opacity: 0}, 0.2), 0);
                stage13.append(TweenMax.from("#stage13_texto_1", .8, {y: -10, opacity: 0}, 0.2), 0);
                stage13.append(TweenMax.from("#stage13_texto_2", .8, {y: -10, opacity: 0}, 0.2), 0);
                stage13.stop();

                stage14.append(TweenMax.from("#stage14", .8, {y: 1300, opacity: 0}), 0);
                stage14.append(TweenMax.from("#stage14_texto", .8, {y: -10, opacity: 0}, 0.2), 0);  
                stage14.append(TweenMax.staggerFrom(".stage14_btns", .8, {x: 130, opacity: 0}, 0.2), 0);
                stage14.stop();

                stage15.append(TweenMax.from("#stage15", .8, {y: 1300, opacity: 0}), 0);
                stage15.append(TweenMax.from("#stage15_texto", .8, {y: -10, opacity: 0}, 0.2), 0);  
                stage15.append(TweenMax.staggerFrom(".stage15_btns", .8, {x: 130, opacity: 0}, 0.2), 0);
                stage15.stop();
                
                stage16.append(TweenMax.from("#stage16", .8, {y: 1300, opacity: 0}), 0);
                stage16.append(TweenMax.from("#stage16_texto", .8, {y: -10, opacity: 0}, 0.2), 0);  
                stage16.append(TweenMax.staggerFrom(".stage16_btns", .8, {x: 130, opacity: 0}, 0.2), 0);
                stage16.stop();
                
                stage17.append(TweenMax.from("#stage17", .8, {y: 1300, opacity: 0}), 0);
                stage17.append(TweenMax.from("#stage17_texto_1", .8, {y: -10, opacity: 0}, 0.2), 0); 
                stage17.append(TweenMax.from("#stage17_btn_menu", .8, {x: 200, opacity: 0}, 0.2), 0);
                stage17.stop();
                
                stage18.append(TweenMax.from("#stage18", .8, {y: 1300, opacity: 0}), 0);                
                stage18.append(TweenMax.staggerFrom(".stage18_btns", .8, {y: 130, opacity: 0}, 0.2), 0);
                stage18.append(TweenMax.from("#stage18_texto_1", .8, {y: -10, opacity: 0}, 0.2), 0);
                stage18.stop();

                stage19.append(TweenMax.from("#stage19", .8, {y: 1300, opacity: 0}), 0);
                stage19.append(TweenMax.from("#stage19_texto", .8, {y: -10, opacity: 0}, 0.2), 0);  
                stage19.append(TweenMax.staggerFrom(".stage19_btns", .8, {x: 130, opacity: 0}, 0.2), 0);
                stage19.stop();
                
                stage20.append(TweenMax.from("#stage20", .8, {y: 1300, opacity: 0}), 0);
                stage20.append(TweenMax.from("#stage20_texto", .8, {y: -10, opacity: 0}, 0.2), 0);  
                stage20.append(TweenMax.staggerFrom(".stage20_btns", .8, {x: 130, opacity: 0}, 0.2), 0);
                stage20.stop();
                
                stage21.append(TweenMax.from("#stage21", .8, {y: 1300, opacity: 0}), 0);
                stage21.append(TweenMax.from("#stage21_texto", .8, {y: -10, opacity: 0}, 0.2), 0);  
                stage21.append(TweenMax.staggerFrom(".stage21_btns", .8, {x: 130, opacity: 0}, 0.2), 0);
                stage21.stop();

                stage22.append(TweenMax.from("#stage22", .8, {y: 1300, opacity: 0}), 0);
                stage22.append(TweenMax.from("#stage22_texto", .8, {y: -10, opacity: 0}, 0.2), 0); 
                stage22.append(TweenMax.from("#stage22_btn_grafico", .8, {y: 10, opacity: 0}, 0.2), 0); 
                stage22.stop();

                stage23.append(TweenMax.from("#stage23", .8, {y: 1300, opacity: 0}), 0);
                stage23.append(TweenMax.from("#stage23_grafico", .8, {y: -10, opacity: 0}, 0.2), 0); 
                stage23.stop();
                
                
                
                
                if (this.movil) {
                    stage1.play();
                    stage2.play();
                    stage3.play();
                    stage4.play();
                    stage5.play();
                    stage6.play();
                    stage6_1.play();
                    stage6_2.play();
                    stage7.play();
                    stage8.play();
                    stage8.play();
                    stage9.play();
                    stage10.play();
                    stage11.play();
                    stage12.play();
                    stage13.play();
                    stage14.play();
                    stage15.play();
                    stage16.play();
                    stage17.play();
                    stage18.play();
                    stage19.play();
                    stage20.play();
                    stage21.play();
                    stage22.play();
                    stage23.play();
                }

            }
        }
    });
}